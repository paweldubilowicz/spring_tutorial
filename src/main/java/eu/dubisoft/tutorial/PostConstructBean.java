package eu.dubisoft.tutorial;

import eu.dubisoft.tutorial.step4.service.Step4PostConstructService;
import eu.dubisoft.tutorial.step5.service.Step5PostConstructService;
import javax.annotation.PostConstruct;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PostConstructBean {

  @Setter(onMethod = @__({@Autowired}))
  private Step4PostConstructService step4PostConstructService;

  @Setter(onMethod = @__({@Autowired}))
  private Step5PostConstructService step5PostConstructService;

  @PostConstruct
  // to wykona sie po zlozeniu struktury projektu, ale przed jego ostatecznym uruchomieniem
  public void init() {
    step4PostConstructService.init();
    step5PostConstructService.init();
  }
}
