package eu.dubisoft.tutorial.step5.api;

import eu.dubisoft.tutorial.step5.dto.IngredientDto;
import eu.dubisoft.tutorial.step5.dto.IngredientForTableDto;
import eu.dubisoft.tutorial.step5.enums.Unit;
import eu.dubisoft.tutorial.step5.service.IngredientService;
import java.util.List;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/step5/ingredients")
@CrossOrigin(origins = "*", allowCredentials = "true", allowedHeaders = "*", methods = {
    RequestMethod.GET, RequestMethod.HEAD, RequestMethod.OPTIONS, RequestMethod.DELETE,
    RequestMethod.POST, RequestMethod.PUT})
public class IngredientController {

  @Setter(onMethod = @__({@Autowired}))
  private IngredientService ingredientService;

  @RequestMapping(value = "/{id}", method = RequestMethod.GET)
  private List<IngredientForTableDto> findAllForTableByRecipeId(@PathVariable("id") Long id) {
    return ingredientService.findAllForTableByRecipeId(id);
  }

  @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
  private void deleteById(@PathVariable("id") Long id) {
    ingredientService.deleteById(id);
  }

  @RequestMapping(value = "", method = RequestMethod.POST,
      consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  private IngredientDto save(@RequestBody IngredientDto ingredient) {
    return ingredientService.save(ingredient);
  }

  // tak zeby nie bylo, nie musimy wcale zwracac rzeczy z bazy. W tym przypadku slownik mamy w enumie, wiec go zwracamy
  @RequestMapping(value = "/units", method = RequestMethod.GET)
  private Unit[] getUnits() {
    return Unit.values();
  }
}
