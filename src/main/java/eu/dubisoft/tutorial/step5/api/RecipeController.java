package eu.dubisoft.tutorial.step5.api;

import eu.dubisoft.tutorial.step5.dto.RecipeDto;
import eu.dubisoft.tutorial.step5.dto.RecipeForTableDto;
import eu.dubisoft.tutorial.step5.service.RecipeService;
import java.util.List;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/step5/recipes")
@CrossOrigin(origins = "*", allowCredentials = "true", allowedHeaders = "*", methods = {
    RequestMethod.GET, RequestMethod.HEAD, RequestMethod.OPTIONS, RequestMethod.DELETE,
    RequestMethod.POST, RequestMethod.PUT})
public class RecipeController {

  @Setter(onMethod = @__({@Autowired}))
  private RecipeService recipeService;

  @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  private List<RecipeForTableDto> findAllForTable() {
    return recipeService.findAllForTable();
  }

  @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  private RecipeDto findOneDetailed(@PathVariable("id") Long id) {
    return recipeService.findOneDetailed(id);
  }

  @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
  private void deleteById(@PathVariable("id") Long id) {
    recipeService.deleteById(id);
  }

  @RequestMapping(value = "", method = RequestMethod.POST,
      consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  private RecipeDto save(@RequestBody RecipeDto recipe) {
    return recipeService.save(recipe);
  }
}
