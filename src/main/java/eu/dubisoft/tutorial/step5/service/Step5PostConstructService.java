package eu.dubisoft.tutorial.step5.service;

import eu.dubisoft.tutorial.step5.entity.Ingredient;
import eu.dubisoft.tutorial.step5.entity.Recipe;
import eu.dubisoft.tutorial.step5.enums.Unit;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Step5PostConstructService {

  @Setter(onMethod = @__({@Autowired}))
  private RecipeService recipeService;

  public void init() {
    createR1();
    createR2();
    createR3();
  }

  private void createR1() {
    Recipe r = new Recipe("Eggs'n'bacon", 1, "Eggs and bacon, doh!");
    r.addIngredient(new Ingredient("Egg", 2, Unit.PIECE));
    r.addIngredient(new Ingredient("Bacon", 1500, Unit.GRAM));
    recipeService.create(r);
  }

  private void createR2() {
    Recipe r = new Recipe("Pancakes", 2, "Derp!");
    r.addIngredient(new Ingredient("Egg", 4, Unit.PIECE));
    r.addIngredient(new Ingredient("Milk", 500, Unit.MILLILITRE));
    recipeService.create(r);
  }

  private void createR3() {
    Recipe r = new Recipe("Dark pot", 5, "The darkness in your pot");
    r.addIngredient(new Ingredient("Things", 20, Unit.PIECE));
    r.addIngredient(new Ingredient("Stuff", 600, Unit.GRAM));
    r.addIngredient(new Ingredient("Whatchummacallit", 200, Unit.MILLILITRE));
    recipeService.create(r);
  }
}
