package eu.dubisoft.tutorial.step5.service;

import eu.dubisoft.tutorial.step5.converter.RecipeConverter;
import eu.dubisoft.tutorial.step5.dto.RecipeDto;
import eu.dubisoft.tutorial.step5.dto.RecipeForTableDto;
import eu.dubisoft.tutorial.step5.entity.Recipe;
import eu.dubisoft.tutorial.step5.repository.RecipeRepository;
import java.util.List;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RecipeService {

  @Setter(onMethod = @__({@Autowired}))
  private RecipeRepository recipeRepository;

  @Setter(onMethod = @__({@Autowired}))
  private RecipeConverter recipeConverter;

  public Recipe create(Recipe recipe) {
    return recipeRepository.save(recipe);
  }

  public List<RecipeForTableDto> findAllForTable() {
    return recipeRepository.findAllForTable();
  }

  public RecipeDto findOneDetailed(Long id) {
    return recipeRepository.findOneDetailed(id);
  }

  public void deleteById(Long id) {
    recipeRepository.deleteById(id);
  }

  // metoda obslugujaca zarowno zapis jak i edycje
  public RecipeDto save(RecipeDto recipe) {
    Recipe entity;
    if (recipe.getId() == null) {
      entity = new Recipe();
    } else {
      entity = recipeRepository.getOne(recipe.getId());
    }
    entity = recipeConverter.updateEntityWithDto(entity, recipe);
    return recipeConverter.entityToDto(recipeRepository.save(entity));
  }
}
