package eu.dubisoft.tutorial.step5.service;

import eu.dubisoft.tutorial.step5.converter.IngredientConverter;
import eu.dubisoft.tutorial.step5.dto.IngredientDto;
import eu.dubisoft.tutorial.step5.dto.IngredientForTableDto;
import eu.dubisoft.tutorial.step5.entity.Ingredient;
import eu.dubisoft.tutorial.step5.entity.Recipe;
import eu.dubisoft.tutorial.step5.repository.IngredientRepository;
import eu.dubisoft.tutorial.step5.repository.RecipeRepository;
import java.util.List;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IngredientService {

  @Setter(onMethod = @__({@Autowired}))
  private IngredientRepository ingredientRepository;

  @Setter(onMethod = @__({@Autowired}))
  private RecipeRepository recipeRepository;

  @Setter(onMethod = @__({@Autowired}))
  private IngredientConverter ingredientConverter;

  public List<IngredientForTableDto> findAllForTableByRecipeId(Long id) {
    return ingredientRepository.findAllForTableByRecipeId(id);
  }

  public void deleteById(Long id) {
    ingredientRepository.deleteById(id);
  }

  // tutaj zapis troche inaczej, entity nadal dodajemy to do relacji 2-stronnej i zapisujemy kaskadowo
  public IngredientDto save(IngredientDto ingredient) {
    if (ingredient.getRecipeId() == null) {
      throw new RuntimeException("Recipe id for ingredient is not present");
    }
    Ingredient entity;
    Recipe recipe;
    if (ingredient.getId() == null) {
      entity = new Ingredient();
      recipe = recipeRepository.getOne(ingredient.getRecipeId());
    } else {
      entity = ingredientRepository.getOne(ingredient.getId());
      recipe = entity.getRecipe();
    }
    entity = ingredientConverter.updateEntityWithDto(entity, ingredient);
    recipe.addIngredient(entity);
    // tutaj robimy troche dziwna operacje, bo koniec koncow zapisujemy skladnik, ktory nie ma kaskady z przepisem
    // robimy tak, bo potrzebujemy w zwrotce ID skladnika, a zapis tego przez zapis przepisu nam tego ID nie da
    // UWAGA: starsze wersje spring-data i JPA moga miec z tym problem, bo na skladniku nie ma kaskady w strone przepisu,
    // w bazie zapis sie powiedzie, co najwyzej entityManager sie pogubi... magia i komplikacja relacji 2-stronnych
    entity = ingredientRepository.save(entity);
    return ingredientConverter.entityToDto(entity);
  }
}
