package eu.dubisoft.tutorial.step5.enums;

public enum Unit {
  PIECE,
  GRAM,
  MILLILITRE
}
