// ----------------------- WIDOK -----------------------------

// tu przechowujemy pobrane z serwera dane
var recipeTableData = [];
var defaultDifficulty = null;

// inicjalizacja zawartosci selecta, tworzenie tabeli i jej eventow, pobranie danych
function initRecipeComponents() {
  createDifficultySelect();
  createRecipeTable();
  createRecipeTableEvents();
  getRecipeDataForTable();
}

function createDifficultySelect() {
// tutaj tworzymy opcje selecta difficulty, wartosc "pod maska" i wartosc wyswietlana
  var difficultyOptions = [
    {value: 1, text: "*"},
    {value: 2, text: "**"},
    {value: 3, text: "***"},
    {value: 4, text: "****"},
    {value: 5, text: "*****"}
  ];
  defaultDifficulty = difficultyOptions[0];
  var modelList = document.getElementById("recipe-difficulty");
  for (var i = 0; i < difficultyOptions.length; i++) {
    modelList.options.add(
        new Option(difficultyOptions[i].text, difficultyOptions[i].value));
  }
}

function createRecipeTable() {
  $(document).ready(function () {
    $('#recipe-table').DataTable({
      data: convertRecipeDataForTable(recipeTableData),
      columns: [
        {title: "Id"},
        {title: "Name"},
        {title: "Diffuculty"}
      ]
    });
  });
}

// zachowanie przy kliknieciu w wiersz
function createRecipeTableEvents() {
  $('#recipe-table').on('click', 'tbody tr', function () {
    var table = $('#recipe-table').dataTable();
    var row = table.fnGetData(this);
    // row zawiera dane calego wiersza, w kolumnie 0 jest przechowywany id, zgodnie z metoda convertRecipeDataForTable
    var response = getRecipeDataForForm(row[0]);
    recipeObjectToForm(response);
  })
}

// odswiezenie tabeli... nieoptymalne ale dziala
function reinitializeRecipeTable() {
  $('#recipe-table').DataTable().destroy();
  createRecipeTable();
}

// setowanie pol w formularzu pustymi wartosciami
function clearRecipeForm() {
  selectedRecipeId = null;
  document.getElementById('recipe-id').value = null;
  document.getElementById('recipe-name').value = null;
  document.getElementById('recipe-difficulty').value = defaultDifficulty;
  document.getElementById('recipe-description').value = null;
}

// wyciaganie danych z formularza do obiektu
function recipeFormToObject() {
  return {
    id: document.getElementById('recipe-id').value,
    name: document.getElementById('recipe-name').value,
    difficulty: document.getElementById('recipe-difficulty').value,
    description: document.getElementById('recipe-description').value
  }
}

// przerzucanie obiektu do formularza
function recipeObjectToForm(data) {
  selectedRecipeId = data.id;
  document.getElementById('recipe-id').value = data.id;
  document.getElementById('recipe-name').value = data.name;
  document.getElementById('recipe-difficulty').value = data.difficulty;
  document.getElementById('recipe-description').value = data.description;
}

// konwersja danych odebranych z backendu na forme przystepna dla jquery.dataTables
function convertRecipeDataForTable(objectList) {
  var arrayList = [];
  for (var i = 0; i < objectList.length; i++) {
    arrayList[i] = [
      objectList[i].id,
      objectList[i].name,
      objectList[i].difficulty
    ];
  }
  return arrayList;
}

function showIngredientsForSelectedRecipe() {
  switchTab('modules/app/ingredients.html')
}

// ----------------------- KOMUNIKACJA -----------------------------

// tutaj korzystamy z przygotowanego recznie wrappera do klienta HTTP w /common/rest-connector.js

function getRecipeDataForTable() {
  var url = HOST + '/step5/recipes';
  var response = httpGet(url);
  recipeTableData = JSON.parse(response);
  reinitializeRecipeTable();
}

function getRecipeDataForForm(id) {
  var url = HOST + '/step5/recipes/' + id;
  var params = '?id=' + id;
  var response = httpGet(url + params);
  return JSON.parse(response);
}

function deleteSelectedRecipe() {
  var id = document.getElementById('recipe-id').value;
  var url = HOST + '/step5/recipes/' + id;
  httpDelete(url);
  clearRecipeForm();
  getRecipeDataForTable();
}

function saveRecipeForm() {
  var formData = recipeFormToObject();
  var url = HOST + '/step5/recipes';
  var response = httpPost(url, formData);
  var responseObject = JSON.parse(response);
  document.getElementById('recipe-id').value = responseObject.id;
  getRecipeDataForTable();
}