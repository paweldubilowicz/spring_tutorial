// ----------------------- WIDOK -----------------------------

var ingredientTableData = [];
var defaultUnit = null;

function initIngredientComponents() {
  createUnitSelect();
  createIngredientTable();
  createIngredientTableEvents();
  getIngredientDataForTable();
}

function createUnitSelect() {
  var units = getUnitDictionary();
  defaultUnit = units[0];
  var modelList = document.getElementById("ingredient-unit");
  for (var i = 0; i < units.length; i++) {
    modelList.options.add(new Option(units[i], units[i]));
  }
}

function createIngredientTable() {
  $(document).ready(function () {
    $('#ingredient-table').DataTable({
      data: convertIngredientDataForTable(ingredientTableData),
      columns: [
        {title: "Id"},
        {title: "Name"},
        {title: "Amount"},
        {title: "Unit"}
      ]
    });
  });
}

function createIngredientTableEvents() {
  $('#ingredient-table').on('click', 'tbody tr', function () {
    var table = $('#ingredient-table').dataTable();
    var row = table.fnGetData(this);
    var response = getIngredientDataForForm(row[0]);
    ingredientObjectToForm(response);
  })
}

// tutaj, poniewaz wszystkie informacje mamy juz na froncie, nie musimy uderzac do backendu,
// musimy tylko poszukac rekordu w tabelce lokalnej
function getIngredientDataForForm(id) {
  for (var i = 0; i < ingredientTableData.length; i++) {
    if (ingredientTableData[i].id == id) {
      return ingredientTableData[i];
    }
  }
  return null;
}

function reinitializeIngredientTable() {
  $('#ingredient-table').DataTable().destroy();
  createIngredientTable();
}

function clearIngredientForm() {
  document.getElementById('ingredient-id').value = null;
  document.getElementById('ingredient-name').value = null;
  document.getElementById('ingredient-amount').value = null;
  document.getElementById('ingredient-unit').value = defaultUnit;
}

function ingredientFormToObject() {
  return {
    id: document.getElementById('ingredient-id').value,
    name: document.getElementById('ingredient-name').value,
    amount: document.getElementById('ingredient-amount').value,
    unit: document.getElementById('ingredient-unit').value,
    recipeId: selectedRecipeId // taki nedzny fixmix - musimy na backendzie wiedziec, do czego to przywiazujemy
  }
}

function ingredientObjectToForm(data) {
  selectedIngredientId = data.id;
  document.getElementById('ingredient-id').value = data.id;
  document.getElementById('ingredient-name').value = data.name;
  document.getElementById('ingredient-amount').value = data.amount;
  document.getElementById('ingredient-unit').value = data.unit;
}

function convertIngredientDataForTable(objectList) {
  var arrayList = [];
  for (var i = 0; i < objectList.length; i++) {
    arrayList[i] = [
      objectList[i].id,
      objectList[i].name,
      objectList[i].amount,
      objectList[i].unit
    ];
  }
  return arrayList;
}

function goToRecipes() {
  selectedRecipeId = null;
  switchTab('modules/app/recipes.html')
}

// ----------------------- KOMUNIKACJA -----------------------------

function getUnitDictionary() {
  var url = HOST + '/step5/ingredients/units';
  var response = httpGet(url);
  return JSON.parse(response);
}

function getIngredientDataForTable() {
  var url = HOST + '/step5/ingredients/' + selectedRecipeId;
  var response = httpGet(url);
  ingredientTableData = JSON.parse(response);
  reinitializeIngredientTable();
}

function deleteSelectedIngredient() {
  var id = document.getElementById('ingredient-id').value;
  var url = HOST + '/step5/ingredients/' + id;
  httpDelete(url);
  clearIngredientForm();
  getIngredientDataForTable();
}

function saveIngredientForm() {
  var formData = ingredientFormToObject();
  var url = HOST + '/step5/ingredients';
  var response = httpPost(url, formData);
  var responseObject = JSON.parse(response);
  document.getElementById('ingredient-id').value = responseObject.id;
  getIngredientDataForTable();
}