function httpGet(fullRequestPathWithParams) {
  var xmlHttp = new XMLHttpRequest();
  console.info('GET ' + fullRequestPathWithParams);
  xmlHttp.open('GET', fullRequestPathWithParams, false);
  xmlHttp.send(null);
  return xmlHttp.response;
}

function httpDelete(fullRequestPathWithParams) {
  var xmlHttp = new XMLHttpRequest();
  console.info('DELETE ' + fullRequestPathWithParams);
  xmlHttp.open('DELETE', fullRequestPathWithParams, false);
  xmlHttp.send(null);
}

function httpPut(fullRequestPath, body) {
  var xmlHttp = new XMLHttpRequest();
  console.info('PUT ' + fullRequestPath);
  xmlHttp.open('PUT', fullRequestPath, false);
  xmlHttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
  xmlHttp.send(JSON.stringify(body));
}

function httpPost(fullRequestPath, body) {
  var xmlHttp = new XMLHttpRequest();
  console.info('POST ' + fullRequestPath);
  xmlHttp.open('POST', fullRequestPath, false);
  xmlHttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
  xmlHttp.send(JSON.stringify(body));
  return xmlHttp.response;
}