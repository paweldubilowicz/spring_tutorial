package eu.dubisoft.tutorial.step5.converter;

import eu.dubisoft.tutorial.step5.dto.RecipeDto;
import eu.dubisoft.tutorial.step5.entity.Recipe;
import org.springframework.stereotype.Component;

@Component
public class RecipeConverter {

  public RecipeDto entityToDto(Recipe in) {
    return new RecipeDto(in.getId(), in.getName(), in.getDifficulty(), in.getDescription());
  }

  public Recipe updateEntityWithDto(Recipe entity, RecipeDto dto) {
    entity.setName(dto.getName());
    entity.setDifficulty(dto.getDifficulty());
    entity.setDescription(dto.getDescription());
    return entity;
  }
}
