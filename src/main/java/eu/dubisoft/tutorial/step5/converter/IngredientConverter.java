package eu.dubisoft.tutorial.step5.converter;

import eu.dubisoft.tutorial.step5.dto.IngredientDto;
import eu.dubisoft.tutorial.step5.entity.Ingredient;
import org.springframework.stereotype.Component;

@Component
public class IngredientConverter {

  public IngredientDto entityToDto(Ingredient in) {
    return new IngredientDto(in.getId(), in.getName(), in.getAmount(), in.getUnit(),
        in.getRecipe().getId());
  }

  public Ingredient updateEntityWithDto(Ingredient entity, IngredientDto dto) {
    entity.setName(dto.getName());
    entity.setAmount(dto.getAmount());
    entity.setUnit(dto.getUnit());
    return entity;
  }
}
