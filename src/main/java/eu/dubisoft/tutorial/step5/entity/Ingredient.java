package eu.dubisoft.tutorial.step5.entity;

import eu.dubisoft.tutorial.step5.enums.Unit;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "ingredients")
@SequenceGenerator(allocationSize = 1, name = "SEQ", sequenceName = "seq_ingredients")
public class Ingredient {

  public Ingredient(String name, Integer amount, Unit unit) {
    this.name = name;
    this.amount = amount;
    this.unit = unit;
  }

  @Id
  @Column(name = "id")
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
  private Long id;

  @Column(name = "create_date")
  @Temporal(TemporalType.TIMESTAMP)
  private Date createDate;

  @Column(name = "update_date")
  @Temporal(TemporalType.TIMESTAMP)
  private Date updateDate;

  @Column(name = "name", nullable = false)
  private String name;

  @Column(name = "amount", nullable = false)
  private Integer amount;

  @Column(name = "unit", nullable = false)
  @Enumerated(EnumType.STRING)
  private Unit unit;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "recipe_id", referencedColumnName = "id", nullable = false)
  private Recipe recipe;

  @PrePersist
  private void onPersist() {
    createDate = new Date();
  }

  @PreUpdate
  private void onUpdate() {
    updateDate = new Date();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Ingredient ingredient = (Ingredient) o;
    return Objects.equals(id, ingredient.id);
  }

  @Override
  public int hashCode() {
    int hash = 7;
    hash = 31 * hash + this.getClass().hashCode();
    hash = 31 * hash + (id != null ? 0 : System.identityHashCode(this));
    hash = 31 * hash + (id != null ? id.hashCode() : 0);
    return hash;
  }
}
