package eu.dubisoft.tutorial.step5.entity;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "recipes")
@SequenceGenerator(allocationSize = 1, name = "SEQ", sequenceName = "seq_recipes")
public class Recipe {

  public Recipe(String name, Integer difficulty, String description) {
    this.name = name;
    this.difficulty = difficulty;
    this.description = description;
  }

  @Id
  @Column(name = "id")
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
  private Long id;

  @Column(name = "create_date")
  @Temporal(TemporalType.TIMESTAMP)
  private Date createDate;

  @Column(name = "update_date")
  @Temporal(TemporalType.TIMESTAMP)
  private Date updateDate;

  @Column(name = "name", nullable = false)
  private String name;

  @Column(name = "difficulty", nullable = false)
  private Integer difficulty;

  // TEXT jest typem bazodanowym (przynajmniej w postgresie) typu LOB
  // String normalnie mapowany jest na varchar(255) z mozliwoscia zmiany maxymalnej dlugosci stringa
  // w przypadku TEXT nie ma limitu co do dlugosci, a dane przechowywane sa w strukturze bazy jako pliki
  @Column(name = "description", columnDefinition = "TEXT", nullable = false)
  private String description;

  // UWAGA: robimy relacje dwustronna. Zapisywac bedziemy jednym ruchem przepis i liste skladnikow, zeby sie dalo, potrzebny jest cascade
  // druga rzecz - zeby dobrze zostalo powiazane musimy dodawac skladniki do zapisu przez wlasna metode (addIngredient)
  // UWAGA: w mappedBy uzywamy nazwy pola, nie nazwy kolumny, czasami w JPA tak jest, warto sprawdzac w opisie parametru/funkcji
  @OneToMany(mappedBy = "recipe", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  private Set<Ingredient> ingredients;

  public void addIngredients(Collection<Ingredient> ingredients) {
    if (ingredients != null) {
      Iterator<Ingredient> iter = ingredients.iterator();

      while (iter.hasNext()) {
        addIngredient(iter.next());
      }
    }
  }

  public void addIngredient(Ingredient ingredient) {
    if (ingredient == null) {
      return;
    }
    if (this.ingredients == null) {
      this.ingredients = new HashSet<>();
    } else {
      this.ingredients = new HashSet<>(this.ingredients);
    }

    if (ingredient.getRecipe() == null) {
      ingredient.setRecipe(this);
    }
    this.ingredients.remove(ingredient);
    this.ingredients.add(ingredient);
  }

  @PrePersist
  private void onPersist() {
    createDate = new Date();
  }

  @PreUpdate
  private void onUpdate() {
    updateDate = new Date();
  }

  // UWAGA: createDate i updateDate sa polami technicznymi, czesto takie twory sa uzywane do kontroli
  // wszelkich dzialan. Dodatkowo moglyby byc informacje o userze ktory stworzyl/updatowal
  // @PrePersist i @PreUpdate powoduja, ze funkcja wykona sie za kazdym razem przy odpowiedniu tworzeniu i updacie
  // dzieki temu nie trzeba sie przejmowac ustawianiem tego recznie

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass() || ((Recipe) o).getId() == null) {
      return false;
    }
    Recipe recipe = (Recipe) o;
    return Objects.equals(id, recipe.id);
  }

  @Override
  public int hashCode() {
    int hash = 7;
    hash = 31 * hash + this.getClass().hashCode();
    hash = 31 * hash + (id != null ? 0 : System.identityHashCode(this));
    hash = 31 * hash + (id != null ? id.hashCode() : 0);
    return hash;
  }

  //UWAGA: customowy equals and hashcode, przy encjach jedyne sensowne porownywanie jest po ID,
  // w przypadku hashcode'a musimy miec tez opcje ze 2 encje z id == null nie sa uznane za to samo
}
