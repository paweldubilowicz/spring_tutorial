package eu.dubisoft.tutorial.step5.repository;

import eu.dubisoft.tutorial.step5.dto.IngredientForTableDto;
import eu.dubisoft.tutorial.step5.entity.Ingredient;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface IngredientRepository extends JpaRepository<Ingredient, Long> {


  @Query(
      "SELECT new eu.dubisoft.tutorial.step5.dto.IngredientForTableDto(i.id, "
          + " i.name, "
          + " i.amount, "
          + " i.unit) "
          + " FROM Ingredient i "
          + " WHERE i.recipe.id = :id "
          + " ORDER BY i.name ASC ")
  List<IngredientForTableDto> findAllForTableByRecipeId(@Param("id") Long recipeId);
}
