package eu.dubisoft.tutorial.step5.repository;

import eu.dubisoft.tutorial.step5.dto.RecipeDto;
import eu.dubisoft.tutorial.step5.dto.RecipeForTableDto;
import eu.dubisoft.tutorial.step5.entity.Recipe;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface RecipeRepository extends JpaRepository<Recipe, Long> {

  @Query("SELECT new eu.dubisoft.tutorial.step5.dto.RecipeForTableDto(r.id, "
      + " r.name, "
      + " r.difficulty) "
      + " FROM Recipe r "
      + " ORDER BY r.name ASC ")
  List<RecipeForTableDto> findAllForTable();

  @Query("SELECT new eu.dubisoft.tutorial.step5.dto.RecipeDto(r.id, "
      + " r.name, "
      + " r.difficulty, "
      + " r.description) "
      + " FROM Recipe r "
      + " WHERE r.id = :id ")
  RecipeDto findOneDetailed(@Param("id") Long id);
}
