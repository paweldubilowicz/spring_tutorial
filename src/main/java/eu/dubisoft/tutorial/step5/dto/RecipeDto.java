package eu.dubisoft.tutorial.step5.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class RecipeDto {

  public RecipeDto(Long id, String name, Integer difficulty, String description) {
    this.id = id;
    this.name = name;
    this.difficulty = difficulty;
    this.description = description;
  }

  private Long id;
  private String name;
  private Integer difficulty;
  private String description;
}
