package eu.dubisoft.tutorial.step5.dto;

import eu.dubisoft.tutorial.step5.enums.Unit;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class IngredientDto {

  public IngredientDto(Long id, String name, Integer amount, Unit unit, Long recipeId) {
    this.id = id;
    this.name = name;
    this.amount = amount;
    this.unit = unit;
    this.recipeId = recipeId;
  }

  private Long id;
  private String name;
  private Integer amount;
  private Unit unit;
  private Long recipeId;
}
