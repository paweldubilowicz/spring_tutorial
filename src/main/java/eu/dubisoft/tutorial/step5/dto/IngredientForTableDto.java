package eu.dubisoft.tutorial.step5.dto;

import eu.dubisoft.tutorial.step5.enums.Unit;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class IngredientForTableDto {

  public IngredientForTableDto(Long id, String name, Integer amount, Unit unit) {
    this.id = id;
    this.name = name;
    this.amount = amount;
    this.unit = unit;
  }

  private Long id;
  private String name;
  private Integer amount;
  private Unit unit;
}
