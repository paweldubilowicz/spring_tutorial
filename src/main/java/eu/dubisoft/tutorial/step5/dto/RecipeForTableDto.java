package eu.dubisoft.tutorial.step5.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class RecipeForTableDto {

  public RecipeForTableDto(Long id, String name, Integer difficulty) {
    this.id = id;
    this.name = name;
    this.difficulty = difficulty;
  }

  private Long id;
  private String name;
  private Integer difficulty;
}
