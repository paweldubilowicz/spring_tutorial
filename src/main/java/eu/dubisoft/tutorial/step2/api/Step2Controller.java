package eu.dubisoft.tutorial.step2.api;

import eu.dubisoft.tutorial.shared.model.MultiplicationDto;
import eu.dubisoft.tutorial.step2.service.Step2Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/step2")
public class Step2Controller {

  //@Autowired jest to adnotacja odpowiadajaca za wstrzykniecie serwisu
  // wstrzykniete moga byc tylko klasy z adnotacja rozszerzajaca @Component
  // np.: @RestController, @Service, @Repository albo sam @Component
  @Autowired
  private Step2Service step2Service; // <--- konwencja nazewnicza, zazwyczaj zachowujemy nazwę beana, tylko zaczynamy z malej litery

  @RequestMapping(value = "/testMe", method = RequestMethod.GET)
  public String testMe() {
    return step2Service.testMe();
  }

  @RequestMapping(value = "/add", method = RequestMethod.GET)
  public Integer add(@RequestParam("valA") Integer valA, @RequestParam("valB") Integer valB) {
    return step2Service.add(valA, valB);
  }

  @RequestMapping(value = "/square/{valA}", method = RequestMethod.GET)
  public Integer square(@PathVariable("valA") Integer valA) {
    return step2Service.square(valA);
  }

  @RequestMapping(value = "/multiply", method = RequestMethod.POST,
      consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public MultiplicationDto multiply(@RequestBody MultiplicationDto multiplicationDto) {
    step2Service.multiply(multiplicationDto);
    return multiplicationDto;
  }
}
