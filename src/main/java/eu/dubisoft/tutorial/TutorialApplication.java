package eu.dubisoft.tutorial;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
@EntityScan(basePackages = {"eu.dubisoft.tutorial"})
public class TutorialApplication {

  public static void main(String[] args) {
    SpringApplication.run(TutorialApplication.class, args);
  }
}
