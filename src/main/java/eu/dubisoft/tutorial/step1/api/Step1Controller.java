package eu.dubisoft.tutorial.step1.api;

import eu.dubisoft.tutorial.shared.model.MultiplicationDto;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController // sygnalizuje, ze klasa jest kontrolerem
@RequestMapping(value = "/step1") // wspolna czesc adresu dla wszystkich metod w kontrolerze
public class Step1Controller {

  @RequestMapping(value = "/testMe", method = RequestMethod.GET)
  // specyficzna koncowka adresu dla metody
  public String testMe() {
    return "I've been tested!";
  }

  @RequestMapping(value = "/add", method = RequestMethod.GET)
  public Integer add(@RequestParam("valA") Integer valA, @RequestParam("valB") Integer valB) {
    // @RequestParam definiuje parametry przyjmowane przez metode
    return valA + valB;
  }

  @RequestMapping(value = "/square/{valA}", method = RequestMethod.GET)
  public Integer square(@PathVariable("valA") Integer valA) {
    // @PathVariable rowniez definiuje parametry przyjmowane przez metode, ale jako czesc sciezki
    return valA * valA;
  }

  @RequestMapping(value = "/multiply", method = RequestMethod.POST,
      consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public MultiplicationDto multiply(@RequestBody MultiplicationDto multiplicationDto) {
    // @RequestBody definiuje parametr wejsciowy jako cos wiecej niz typy proste
    multiplicationDto.setResult(multiplicationDto.getValA() * multiplicationDto.getValB());
    return multiplicationDto;
  }
}
