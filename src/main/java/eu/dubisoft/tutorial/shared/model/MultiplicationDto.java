package eu.dubisoft.tutorial.shared.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MultiplicationDto {

  private Integer valA;
  private Integer valB;
  private Integer result;
}
