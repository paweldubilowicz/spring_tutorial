package eu.dubisoft.tutorial.step3.bo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "movies")
@SequenceGenerator(allocationSize = 1, name = "SEQ", sequenceName = "seq_movies")
public class MovieBo {

  @Id
  @Column(name = "id")
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
  private Long id;

  @Column(name = "name")
  private String name;

  @Column(name = "year")
  private Integer year;
}
