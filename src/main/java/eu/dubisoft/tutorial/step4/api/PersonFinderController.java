package eu.dubisoft.tutorial.step4.api;

import eu.dubisoft.tutorial.step4.bo.PersonBo;
import eu.dubisoft.tutorial.step4.dto.LightPersonDto;
import eu.dubisoft.tutorial.step4.dto.PersonWithAddressDto;
import eu.dubisoft.tutorial.step4.dto.PersonWithCityDto;
import eu.dubisoft.tutorial.step4.enums.ESkill;
import eu.dubisoft.tutorial.step4.service.PersonFinderService;
import java.util.Date;
import java.util.List;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/step4/person")
public class PersonFinderController {

  @Setter(onMethod = @__({@Autowired}))
  private PersonFinderService personfinderService;

  @RequestMapping(value = "/findByLastName", method = RequestMethod.GET)
  public List<LightPersonDto> findByLastName(@RequestParam("lastName") String lastName) {
    return personfinderService.findByLastName(lastName);
  }

  @RequestMapping(value = "/findByAddress", method = RequestMethod.GET)
  public List<PersonWithAddressDto> findByAddress(@RequestParam("address") String address) {
    return personfinderService.findByAddress(address);
  }

  // enumy sa w wielu przypadkach traktowane jako stringi
  // daty zawsze sa problematycznym typem danych, wiec zazwyczaj wymagaja dodatkowych akcji
  @RequestMapping(value = "/findBySkillAndBornBefore", method = RequestMethod.GET)
  public List<PersonWithCityDto> findBySkillAndBornBefore(@RequestParam("skill") ESkill skill,
      @RequestParam("maxDob") @DateTimeFormat(pattern = "yyyy-MM-dd") Date maxDob) {
    return personfinderService.findBySkillAndBornBefore(skill, maxDob);
  }

  // cale to wywolanie jest zlym pomyslem, wiec wypluwam z aplikacji BO zeby bylo jeszcze gorzej
  @RequestMapping(value = "/findByFirstNameIsIgnoreCaseAndDobBeforeOrderByIdAsc", method = RequestMethod.GET)
  public List<PersonBo> findByFirstNameIsIgnoreCaseAndDobBeforeOrderByIdAsc(
      @RequestParam("firstName") String firstname,
      @RequestParam("maxDob") @DateTimeFormat(pattern = "yyyy-MM-dd") Date maxDob) {
    return personfinderService.findByFirstNameIsIgnoreCaseAndDobBeforeOrderByIdAsc(firstname, maxDob);
  }
}
