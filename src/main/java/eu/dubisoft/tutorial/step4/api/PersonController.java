package eu.dubisoft.tutorial.step4.api;

import eu.dubisoft.tutorial.step4.dto.CompletePersonDto;
import eu.dubisoft.tutorial.step4.service.PersonService;
import java.util.List;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/step4/person")
public class PersonController {

  // moja ulubiona forma wstrzykiwania, ciezka do zapamietania, ale dobrze sie sprawdza
  @Setter(onMethod = @__({@Autowired}))
  private PersonService personService;

  @RequestMapping(value = "/findAll", method = RequestMethod.GET)
  public List<CompletePersonDto> findAll() {
    return personService.findAll();
  }

  @RequestMapping(value = "/find/{id}", method = RequestMethod.GET)
  public CompletePersonDto findAll(@PathVariable("id") Long id) {
    return personService.findOne(id);
  }

  @RequestMapping(value = "/save", method = RequestMethod.POST)
  public CompletePersonDto save(@RequestBody CompletePersonDto in) {
    return personService.save(in);
  }
}
