package eu.dubisoft.tutorial.step4.bo;

import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "persons")
@SequenceGenerator(allocationSize = 1, name = "SEQ", sequenceName = "seq_persons")
public class PersonBo {

  @Id
  @Column(name = "id")
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
  private Long id;

  @Column(name = "first_name")
  private String firstName;

  @Column(name = "last_name")
  private String lastName;

  @Column(name = "dob")
  @Temporal(TemporalType.TIMESTAMP)
  private Date dob; // date of birth

  @OneToOne
  @JoinColumn(name = "address_id")
  private AddressBo address;

  @OneToMany(fetch = FetchType.EAGER) // to zasluguje na kompletnie oddzielny wyklad (appendix_jpa_fetch.txt)
  @JoinColumn(name = "person_id")
  private List<SkillBo> skills;
}
