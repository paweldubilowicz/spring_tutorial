package eu.dubisoft.tutorial.step4.bo;

import eu.dubisoft.tutorial.step4.enums.ESkill;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "skills")
@SequenceGenerator(allocationSize = 1, name = "SEQ", sequenceName = "seq_skills")
public class SkillBo {

  public SkillBo() {
  }

  public SkillBo(ESkill skill) {
    this.skill = skill;
  }

  @Id
  @Column(name = "id")
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
  private Long id;

  @Column(name = "skill")
  @Enumerated(EnumType.STRING)
  private ESkill skill;
}
