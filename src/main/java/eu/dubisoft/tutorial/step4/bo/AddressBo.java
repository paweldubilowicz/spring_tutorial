package eu.dubisoft.tutorial.step4.bo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "addresses")
@SequenceGenerator(allocationSize = 1, name = "SEQ", sequenceName = "seq_addresses")
public class AddressBo {

  @Id
  @Column(name = "id")
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
  private Long id;

  @Column(name = "city")
  private String city;

  @Column(name = "street")
  private String street;
}
