package eu.dubisoft.tutorial.step4.enums;

public enum ESkill {
  JAVA,
  JS,
  DOT_NET;
}
