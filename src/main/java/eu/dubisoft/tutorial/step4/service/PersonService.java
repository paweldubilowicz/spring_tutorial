package eu.dubisoft.tutorial.step4.service;

import eu.dubisoft.tutorial.step4.bo.PersonBo;
import eu.dubisoft.tutorial.step4.converter.CompletePersonConverter;
import eu.dubisoft.tutorial.step4.dto.CompletePersonDto;
import eu.dubisoft.tutorial.step4.repository.IAddressRepository;
import eu.dubisoft.tutorial.step4.repository.IPersonRepository;
import eu.dubisoft.tutorial.step4.repository.ISkillRepository;
import java.util.List;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

@Service
public class PersonService {

  @Setter(onMethod = @__({@Autowired}))
  private CompletePersonConverter completePersonConverter;

  @Setter(onMethod = @__({@Autowired}))
  private IPersonRepository personRepository;

  @Setter(onMethod = @__({@Autowired}))
  private IAddressRepository addressRepository;

  @Setter(onMethod = @__({@Autowired}))
  private ISkillRepository skillRepository;

  public List<CompletePersonDto> findAll() {
    List<PersonBo> persons = personRepository
        .findAll(Sort.by(Direction.ASC, "id")); // pobieramy z bazy
    // zawsze przy pobieraniu powinnismy definiowac jakikolwiek sort, inaczej kolejnosc rekordow MOZE byc randomowa
    List<CompletePersonDto> completePersonDtoList = completePersonConverter
        .boListToDtolist(persons); // konwertujemy
    // encje (BO) nigdy nie powinny wychodzic poza warstwe serwisow... czyli nigdy do API
    return completePersonDtoList;
  }

  public CompletePersonDto findOne(Long id) {
    return completePersonConverter
        .boToDto(personRepository.findById(id).orElse(null)); // wszystko w jednej linijce
    // findById jest w nowej wersji nieco wkurzajace, entity zwraca Optional<?>, dlatego uzylem orElse
  }

  public CompletePersonDto save(CompletePersonDto in) {
    PersonBo person = completePersonConverter.dtoToBo(in); // konwersja na BO
    // fajnie byloby zrobic zapis osoby wraz z relacjami, ale brak kaskady to uniemozliwia
    // najpierw trzeba pozapisywac obiekty powiazane (adres i skille), nastepnie wstawic je
    // spowrotem do osoby i dopiero zapisac
    person.setAddress(addressRepository.save(person.getAddress())); // zapis i podstawienie adresu
    person.setSkills(skillRepository.saveAll(person.getSkills())); // zapis i podstawienie skilli
    person = personRepository.save(person); // zapis osoby
    return completePersonConverter.boToDto(person); // konwersja i zwrot
  }
}
