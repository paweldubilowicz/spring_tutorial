package eu.dubisoft.tutorial.step4.service;

import eu.dubisoft.tutorial.step4.bo.AddressBo;
import eu.dubisoft.tutorial.step4.bo.PersonBo;
import eu.dubisoft.tutorial.step4.bo.SkillBo;
import eu.dubisoft.tutorial.step4.enums.ESkill;
import eu.dubisoft.tutorial.step4.repository.IAddressRepository;
import eu.dubisoft.tutorial.step4.repository.IPersonRepository;
import eu.dubisoft.tutorial.step4.repository.ISkillRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Step4PostConstructService {

  @Autowired // tutaj robimy wstrzykniecie przez konstruktor, nie jestem fanem, sa lepsze sposoby
  public Step4PostConstructService(IPersonRepository personRepository,
      IAddressRepository addressRepository,
      ISkillRepository skillRepository) {
    this.personRepository = personRepository;
    this.addressRepository = addressRepository;
    this.skillRepository = skillRepository;
  }

  private final IPersonRepository personRepository;
  private final IAddressRepository addressRepository;
  private final ISkillRepository skillRepository;

  public void init() {
    createPerson1();
    createPerson2();
    createPerson3();
  }

  private void createPerson1() {
    PersonBo person = new PersonBo();
    person.setFirstName("John");
    person.setLastName("Smith");
    Calendar c = Calendar.getInstance();
    c.set(1980, Calendar.OCTOBER, 10, 0, 0, 0);
    person.setDob(c.getTime());

    AddressBo address = new AddressBo();
    address.setCity("London");
    address.setStreet("Long Street");
    address = addressRepository.save(address); // najpierw zapisujemy adres

    person.setAddress(address); // nastepnie doklejamy go do osoby

    List<SkillBo> skills = new ArrayList<>(); // tworzymy kilka skilli
    skills.add(new SkillBo(ESkill.JAVA));
    skills.add(new SkillBo(ESkill.JS));

    skillRepository.saveAll(skills); // zapisujemy skille

    person.setSkills(skills); // doklejamy skille do osoby

    personRepository.save(person); // w koncu zapisujemy osobe

    // mozna by to bylo zrobic jednym zapisem poprzez ustawienie kaskad w encji, ale przy kaskadach latwo o bledy
  }

  private void createPerson2() {
    PersonBo person = new PersonBo();
    person.setFirstName("John");
    person.setLastName("Novak");
    Calendar c = Calendar.getInstance();
    c.set(1974, Calendar.MARCH, 1, 0, 0, 0);
    person.setDob(c.getTime());

    AddressBo address = new AddressBo();
    address.setCity("Chicago");
    address.setStreet("Short Street");
    address = addressRepository.save(address);

    person.setAddress(address);

    List<SkillBo> skills = new ArrayList<>();
    skills.add(new SkillBo(ESkill.DOT_NET));
    skills.add(new SkillBo(ESkill.JS));

    skillRepository.saveAll(skills);

    person.setSkills(skills);

    personRepository.save(person);
  }

  private void createPerson3() {
    PersonBo person = new PersonBo();
    person.setFirstName("Anna");
    person.setLastName("Novak");
    Calendar c = Calendar.getInstance();
    c.set(1974, Calendar.APRIL, 22, 0, 0, 0);
    person.setDob(c.getTime());

    AddressBo address = new AddressBo();
    address.setCity("Chicago");
    address.setStreet("Short Street");
    address = addressRepository.save(address);

    person.setAddress(address);

    List<SkillBo> skills = new ArrayList<>();
    skills.add(new SkillBo(ESkill.JS));
    skills = skillRepository.saveAll(skills);

    person.setSkills(skills);

    personRepository.save(person);
  }
}
