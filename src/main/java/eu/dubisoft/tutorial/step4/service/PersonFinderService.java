package eu.dubisoft.tutorial.step4.service;

import eu.dubisoft.tutorial.step4.bo.PersonBo;
import eu.dubisoft.tutorial.step4.dto.LightPersonDto;
import eu.dubisoft.tutorial.step4.dto.PersonWithAddressDto;
import eu.dubisoft.tutorial.step4.dto.PersonWithCityDto;
import eu.dubisoft.tutorial.step4.enums.ESkill;
import eu.dubisoft.tutorial.step4.repository.IPersonRepository;
import java.util.Date;
import java.util.List;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonFinderService {

  // w tym serwisie pomijam konwertery, nie zawsze trzeba wyciagac z bazy BO.
  // Czasami mozna prosto do DTO albo projekcji wyciagnac tylko potrzebne dane, ale nie zawsze

  @Setter(onMethod = @__({@Autowired}))
  private IPersonRepository personRepository;


  public List<LightPersonDto> findByLastName(String lastName) {
    return personRepository.findbyLastname(lastName);
  }

  public List<PersonWithAddressDto> findByAddress(String address) {
    return personRepository.findByAddress(address);
  }

  public List<PersonWithCityDto> findBySkillAndBornBefore(ESkill skill, Date maxDob) {
    return personRepository.findBySkillAndBornBefore(skill, maxDob);
  }

  public List<PersonBo> findByFirstNameIsIgnoreCaseAndDobBeforeOrderByIdAsc(String firstname, Date maxDob) {
    return personRepository.findByFirstNameIsIgnoreCaseAndDobBeforeOrderByIdAsc(firstname, maxDob);
  }
}
