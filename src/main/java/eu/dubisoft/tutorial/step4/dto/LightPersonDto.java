package eu.dubisoft.tutorial.step4.dto;

import java.util.Date;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class LightPersonDto {

  // mozna uzyc @AllArgsConstructor, ale nie lubie tej opcji, entity nie moge gwarantowac zamierzonej kolejnosci argumentow
  public LightPersonDto(Long id, String firstName, String lastName, Date dob) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.dob = dob;
  }

  private Long id;
  private String firstName;
  private String lastName;
  private Date dob;
}
