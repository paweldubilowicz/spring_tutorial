package eu.dubisoft.tutorial.step4.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class PersonWithCityDto {

  public PersonWithCityDto(String firstName, String lastName, String city) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.city = city;
  }

  private String firstName;
  private String lastName;
  private String city;
}
