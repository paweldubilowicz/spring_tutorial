package eu.dubisoft.tutorial.step4.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class PersonWithAddressDto {

  public PersonWithAddressDto(Long id, String firstName, String lastName, String address) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.address = address;
  }

  private Long id;
  private String firstName;
  private String lastName;
  private String address;
}