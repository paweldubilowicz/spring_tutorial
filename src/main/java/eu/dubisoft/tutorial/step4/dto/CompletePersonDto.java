package eu.dubisoft.tutorial.step4.dto;

import eu.dubisoft.tutorial.step4.enums.ESkill;
import java.util.Date;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CompletePersonDto {

  private Long id;
  private String firstName;
  private String lastName;
  private Date dob;
  private String city;
  private String street;
  private List<ESkill> skills;
}
