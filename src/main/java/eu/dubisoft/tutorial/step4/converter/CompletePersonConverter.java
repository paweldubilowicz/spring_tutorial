package eu.dubisoft.tutorial.step4.converter;

import eu.dubisoft.tutorial.step4.bo.AddressBo;
import eu.dubisoft.tutorial.step4.bo.PersonBo;
import eu.dubisoft.tutorial.step4.bo.SkillBo;
import eu.dubisoft.tutorial.step4.dto.CompletePersonDto;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

@Component
public class CompletePersonConverter {

  // mocno lopatologiczna klasa konwertujaca

  public List<CompletePersonDto> boListToDtolist(List<PersonBo> ins) {
    return ins.stream().map(this::boToDto).collect(Collectors.toList());
  }

  public CompletePersonDto boToDto(PersonBo in) {
    CompletePersonDto out = new CompletePersonDto();
    out.setId(in.getId());
    out.setFirstName(in.getFirstName());
    out.setLastName(in.getLastName());
    out.setDob(in.getDob());
    out.setCity(in.getAddress().getCity());
    out.setStreet(in.getAddress().getStreet());
    out.setSkills(in.getSkills().stream().map(SkillBo::getSkill).collect(Collectors.toList()));
    return out;
  }

  public PersonBo dtoToBo(CompletePersonDto in) {
    PersonBo out = new PersonBo();
    out.setId(in.getId());
    out.setFirstName(in.getFirstName());
    out.setLastName(in.getLastName());
    out.setDob(in.getDob());

    AddressBo address = new AddressBo();
    address.setCity(in.getCity());
    address.setStreet(in.getStreet());
    out.setAddress(address);

    out.setSkills(in.getSkills().stream().map(SkillBo::new).collect(Collectors.toList()));

    return out;
  }
}
