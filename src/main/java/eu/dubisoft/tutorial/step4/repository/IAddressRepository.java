package eu.dubisoft.tutorial.step4.repository;

import eu.dubisoft.tutorial.step4.bo.AddressBo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IAddressRepository extends JpaRepository<AddressBo, Long> {

}
