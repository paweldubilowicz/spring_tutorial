package eu.dubisoft.tutorial.step4.repository;

import eu.dubisoft.tutorial.step4.bo.PersonBo;
import eu.dubisoft.tutorial.step4.dto.LightPersonDto;
import eu.dubisoft.tutorial.step4.dto.PersonWithAddressDto;
import eu.dubisoft.tutorial.step4.dto.PersonWithCityDto;
import eu.dubisoft.tutorial.step4.enums.ESkill;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface IPersonRepository extends JpaRepository<PersonBo, Long> {
  // JPQL jest problematycznym jezykiem, jest do SQL z dosyc ograniczonymi funkcjonalnosciami i
  // do poziomu absurdu polegajacym na strukturze obiektow
  // Ponadto repozytoria tez nie sa najlepszym rozwiazaniem, mozna sobie zycie ulatwic, ale bardziej
  // zlozonych metod po prostu nie da sie tutaj zrobic

  // tutaj uzywamy bezposredniego wyciagniecia pol i wpakowania ich w konstruktor klasy
  // jakbysmy chcieli wyciagnac cala encje zrobilibysmy w tym przypadku SELECT p, metoda musialaby zwracac List<PersonBo>
  @Query(
      "SELECT new eu.dubisoft.tutorial.step4.dto.LightPersonDto(p.id, p.firstName, p.lastName, p.dob) "
          + " FROM PersonBo p "
          + " WHERE lower(p.lastName) = lower(:lastName) "
          + " ORDER BY p.id ASC ")
  // jak mowilem, zawsze warto sorta uzyc, SQL sortuje "blizej nieokreslonym algorytmem" wg tworcow
  List<LightPersonDto> findbyLastname(@Param("lastName") String lastName);

  // tutaj w konstruktor wcisnalem jeszcze funkcje laczaca, entity moge
  @Query(
      "SELECT new eu.dubisoft.tutorial.step4.dto.PersonWithAddressDto(p.id, p.firstName, p.lastName, concat(a.city, ', ', a.street)) "
          + " FROM PersonBo p "
          + " LEFT JOIN p.address a "
          // dolaczanie dziala mocno inaczej niz w SQLu, tutaj musimy isc po sciezce pol w klasach
          + " WHERE lower(a.city) like concat('%', lower(:address), '%') "
          + "   OR lower(a.street) like concat('%', lower(:address), '%') "
          + " ORDER BY p.id ASC ")
  List<PersonWithAddressDto> findByAddress(@Param("address") String address);

  // nic nowego, 2 relacje na raz, 2 parametry
  @Query(
      "SELECT new eu.dubisoft.tutorial.step4.dto.PersonWithCityDto(p.firstName, p.lastName, a.city) "
          + " FROM PersonBo p "
          + " LEFT JOIN p.address a "
          + " LEFT JOIN p.skills s "
          + " WHERE s.skill = :skill "
          + " AND p.dob < :maxDob "
          + " ORDER BY p.id ASC ")
  List<PersonWithCityDto> findBySkillAndBornBefore(@Param("skill") ESkill skill,
      @Param("maxDob") Date maxDob);

  // czyste spring-data, nie trzeba pisac SQLa, kompilator sam z nazwy metody wywniskuje co zrobic
  // bardzo ograniczone w swoich mozliwosciach, praktycznie 0 zastosowan w praktyce
  List<PersonBo> findByFirstNameIsIgnoreCaseAndDobBeforeOrderByIdAsc(String firstName, Date maxDob);
}
