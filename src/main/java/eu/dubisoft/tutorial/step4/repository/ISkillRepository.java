package eu.dubisoft.tutorial.step4.repository;

import eu.dubisoft.tutorial.step4.bo.SkillBo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ISkillRepository extends JpaRepository<SkillBo, Long> {

}
